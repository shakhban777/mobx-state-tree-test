import { Instance, types } from "mobx-state-tree"

export const Invoice = types
	.model('Invoice', {
		currency: types.string,
		is_paid: false
	})
	.views(self => ({
		status(): string {
			return self.is_paid ? "Paid" : "Not paid"
		}
	}))

export interface IInvoice extends Instance<typeof Invoice> {}