import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/app'
import './index.css'
import { Invoice } from "./models/invoice"

const invoice = Invoice.create({currency: 'RUS'})

ReactDOM.render(
  <React.StrictMode>
    <App invoice={invoice} />
  </React.StrictMode>,
  document.getElementById('root')
)