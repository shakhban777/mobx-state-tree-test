import { FC } from "react"
import { IInvoice } from "../models/invoice";

type Props = {
	invoice: IInvoice
}

const App: FC<Props> = ({ invoice }) => {

	return (
		<div>
			<h1>{invoice.status()}</h1>
		</div>
	)
}

export default App